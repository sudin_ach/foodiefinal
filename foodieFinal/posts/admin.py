from __future__ import absolute_import

from django.contrib import admin

from .models import Post, Like, Dislike, Comment


class PostAdmin(admin.ModelAdmin):
        list_display = ('slug',
                        'caption',
                        'author',
                        'date_created',
                        'date_updated', )

class LikeAdmin(admin.ModelAdmin):
 list_display = [field.name for field in Like._meta.fields]

admin.site.register(Post, PostAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(Dislike)
admin.site.register(Comment)
