# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('posts', '0005_dislike'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('comment', models.TextField()),
                ('author', models.ForeignKey(related_name='author', to=settings.AUTH_USER_MODEL)),
                ('post', models.ForeignKey(related_name='comment_post', to='posts.Post')),
            ],
        ),
        migrations.RemoveField(
            model_name='dislike',
            name='post',
        ),
        migrations.RemoveField(
            model_name='dislike',
            name='user',
        ),
        migrations.DeleteModel(
            name='DisLike',
        ),
    ]
