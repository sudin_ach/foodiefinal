# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('posts', '0004_auto_20180819_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='DisLike',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('post', models.ForeignKey(related_name='disliked_post', to='posts.Post')),
                ('user', models.ForeignKey(related_name='disliker', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
