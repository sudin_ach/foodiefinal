from django.shortcuts import render
from posts import models as pm
from . import models as rm 
# Create your views here.

def explore_post_view(request):
	path = 'explore/explore.html'
	ratings = rm.Rating.objects.all()
	posts = pm.Post.objects.all()
	var = {
		'ratings':ratings,
		'posts':posts
	}
	return render(request, path, var)
