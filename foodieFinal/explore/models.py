from django.db import models
from posts import models as pm

# Create your models here.

class Rating(models.Model):
	post = models.ForeignKey(pm.Post, unique = True)
	rate = models.IntegerField(default = 10)

	def __str__(self):
		return '{}:{}'.format(self.post, self.rate)

	class Meta:
		ordering = ['-rate']