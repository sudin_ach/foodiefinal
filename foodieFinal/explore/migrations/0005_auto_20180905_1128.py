# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('explore', '0004_rating'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rating',
            options={'ordering': ['-rate']},
        ),
        migrations.AlterField(
            model_name='rating',
            name='post',
            field=models.ForeignKey(unique=True, to='posts.Post'),
        ),
    ]
