# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0009_auto_20180831_0817'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('rate', models.IntegerField()),
                ('dislikes', models.ForeignKey(related_name='rated_dislike', to='posts.Dislike')),
                ('likes', models.ForeignKey(related_name='rated_likes', to='posts.Like')),
                ('post', models.ForeignKey(related_name='rated_post', to='posts.Post')),
            ],
        ),
    ]
