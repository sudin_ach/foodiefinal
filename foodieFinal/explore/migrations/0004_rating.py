# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0009_auto_20180831_0817'),
        ('explore', '0003_auto_20180904_1011'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('rate', models.IntegerField(default=10)),
                ('post', models.ForeignKey(to='posts.Post')),
            ],
        ),
    ]
