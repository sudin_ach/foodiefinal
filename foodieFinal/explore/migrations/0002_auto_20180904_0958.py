# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('explore', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='dislikes',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='rating',
            name='likes',
            field=models.IntegerField(),
        ),
    ]
