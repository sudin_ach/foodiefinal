# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_connection'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='dp',
            field=models.FileField(default='dp/default_dp.jpg', upload_to='dp'),
        ),
    ]
